﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRModel {
[RequireComponent(typeof(MeshFilter))]
	public class MeshEditor : MonoBehaviour {
		public static MeshEditor singleton;
		public int nextVertexID = 0;

		[Header("Renderer Prefabs")]
		public GameObject VertexRendererPrefab;
		public GameObject MultiVertexRendererPrefab;

		public Mesh meshToEdit;
		[ReadOnly] public Mesh editingCopy;

		[Header("Editing Data")]
		protected List<Vertex> l_vertices = new List<Vertex>();

		private void Awake(){
			singleton = this;
		}

		public void Start(){
			//mesh = GenerateCube(3, 2, 0.5f);
			StartMeshEdit(meshToEdit);
		}

		private void Update(){

		}

		public void AddVertex(Vertex _vertex){
			l_vertices.Add(_vertex);
			_vertex.VertexID = nextVertexID++;
		}

		public virtual Mesh GenerateCube(float width, float height, float depth){
			Mesh cube = new Mesh();
			cube.vertices = new Vector3[] {
				new Vector3(width/2,  height/2, depth/2), 	new Vector3(width/2,  height/2, -depth/2), new Vector3(-width/2,  height/2, -depth/2), new Vector3(-width/2, height/2, depth/2),
				new Vector3(width/2, -height/2, depth/2), 	new Vector3(width/2, -height/2, -depth/2), new Vector3(-width/2, -height/2, -depth/2), new Vector3(-width/2, -height/2, depth/2)
				};

			cube.triangles = new int[] { 
				0,1,2, 0,2,3, // top
				0,4,5, 0,5,1, // side 1
				1,5,6, 1,6,2, // side 2
				2,7,3, 2,6,7, // side 3
				3,4,0, 3,7,4, // side 4
				4,6,5, 4,7,6 // bottom
				};

			cube.uv = new Vector2[] {
				new Vector2(0,0),
				new Vector2(1,0),
				new Vector2(0.5f,0),
				new Vector2(0.5f,0),
				new Vector2(0.5f,1),
				new Vector2(0,0.5f),
				new Vector2(0,0.5f),
				new Vector2(0.5f,0.5f)
			};

			cube.normals = new Vector3[]{
				new Vector3(0,0,0),
				new Vector3(0,0,0),
				new Vector3(0,0,0),
				new Vector3(0,0,0),
				new Vector3(0,0,0),
				new Vector3(0,0,0),
				new Vector3(0,0,0),
				new Vector3(0,0,0),
			};
			cube.RecalculateNormals();
			return cube;
		}

		private void StartMeshEdit(Mesh _mesh){
			editingCopy = CopyMesh(_mesh);
			editingCopy.RecalculateNormals();
			for(int i = 0; i < editingCopy.vertices.Length; ++i){
				Vertex newVertex = new Vertex();

				newVertex.VertexID = i;
				newVertex.position = editingCopy.vertices[i];
				newVertex.normal = editingCopy.normals[i];

				newVertex.meshEditor = this;

				VertexRenderer newVertexRenderer = CreateRenderer(newVertex);

				l_vertices.Add(newVertex);
			}
			GetComponent<MeshFilter>().mesh = editingCopy;
		}

		private VertexRenderer CreateRenderer(Vertex vertex){
			GameObject newObject = Instantiate(VertexRendererPrefab);
			VertexRenderer newRenderer = newObject.AddComponent<VertexRenderer>();
			newRenderer.transform.SetParent(transform);
			newRenderer.transform.forward = vertex.normal;
			newRenderer.name = "VertexRenderer - " + vertex.VertexID;
			newRenderer.SetVertex(vertex);

			foreach(Vertex other in l_vertices){
				if(vertex.position == other.position){
					CreateMultiVertexRenderer(vertex, other);
				}
			}
			return newRenderer;
		}

		private MultiVertexRenderer CreateMultiVertexRenderer(Vertex newVertex, Vertex otherVertex){
			MultiVertexRenderer result;
			if(otherVertex.renderer.transform.parent.GetComponent<MultiVertexRenderer>()){
				result = otherVertex.renderer.transform.parent.GetComponent<MultiVertexRenderer>();
				result.AddRenderer(newVertex.renderer);
			} else {
				GameObject newObject = Instantiate(MultiVertexRendererPrefab);
				result = newObject.AddComponent<MultiVertexRenderer>();
				result.AddRenderer(newVertex.renderer);
				result.AddRenderer(otherVertex.renderer);
				result.transform.SetParent(transform);
				result.transform.localPosition = newVertex.position;
				result.transform.forward = newVertex.normal;
			}
			return result;
		}

		public void EditVertex(Vertex _vertex){
			Vector3[] vertices = editingCopy.vertices;
			vertices[_vertex.VertexID] = _vertex.position;
			Vector3[] normals = editingCopy.normals;
			normals[_vertex.VertexID] = _vertex.normal;
			editingCopy.vertices = vertices;
			editingCopy.normals = normals;
		}

		public static Mesh CopyMesh(Mesh original){
			Mesh result = new Mesh();
			result.vertices = original.vertices;
			result.triangles = original.triangles;
			result.normals = original.normals;
			result.tangents = original.tangents;
			result.uv = original.uv;
			result.uv2 = original.uv2;
			//result.uv3 = original.uv3;
			//result.uv4 = original.uv4;
			//result.uv5 = original.uv5;
			//result.uv6 = original.uv6;
			//result.uv7 = original.uv7;
			//result.uv8 = original.uv8;
			result.colors = original.colors;
			result.name = original.name + " - Copy";
			return result;
		}
	}
}