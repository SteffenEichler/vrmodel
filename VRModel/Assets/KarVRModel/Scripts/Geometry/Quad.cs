﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRModel {
[System.Serializable]
public class Quad  {
	public Vertex[] vertexInstances;
	public Vector3[] vertices {
		get {
			Vector3[] result = new Vector3[4];
			for (int i = 0; i < result.Length; i++)
			{
				result[i] = vertexInstances[i].position;
			}
			return result;
		}
	}

	public int[] triangles {
		get {
			return new int[] { 
				vertexInstances[0].VertexID, vertexInstances[1].VertexID, vertexInstances[2].VertexID,
				vertexInstances[0].VertexID, vertexInstances[2].VertexID, vertexInstances[3].VertexID 	};
		}
	}

	public Quad ( Vertex[] _vertices){
		vertexInstances = _vertices;
	}

	public Quad (MeshEditor editor, float width, float height, Vector3 pos, Vector3 rot){
	}

	public Quad(Vertex _v1, Vertex _v2, Vertex _v3, Vertex _v4){
		vertexInstances = new Vertex[] {_v1, _v2, _v3, _v4};
	}

	public static Vector3[] GetVector3sFromVertexInstances(params Vertex[] _vertexInstances){
		List<Vector3> result = new List<Vector3>();
		foreach(Vertex vertex in _vertexInstances){
			result.Add(vertex.position);
		}
		return result.ToArray();
	}
}
}
