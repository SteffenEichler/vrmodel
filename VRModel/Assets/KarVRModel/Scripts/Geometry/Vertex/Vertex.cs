﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRModel {
[System.Serializable]
    public class Vertex {
        #region Editing
        public MeshEditor meshEditor;
        public VertexRenderer renderer;
        #endregion

        public int VertexID;

        public Vector3 position = new Vector3();
        public Vector3 normal = new Vector3();
        
        public Vertex(){
        }

        public Vertex (float x, float y, float z){
            position = new Vector3(x, y, z);
        }

        public void OnMove(){
            if(meshEditor != null)
                meshEditor.EditVertex(this);
        }

        public void OnRotate(){
            if(meshEditor != null)
                meshEditor.EditVertex(this);
        }
    }
}