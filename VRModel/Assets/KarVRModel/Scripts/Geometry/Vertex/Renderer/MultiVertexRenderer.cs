﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace VRModel {
	public class MultiVertexRenderer : MonoBehaviour {
		
		private List<VertexRenderer> l_renderers = new List<VertexRenderer>();

		public void AddRenderer(VertexRenderer _renderer){
			l_renderers.Add(_renderer);
			_renderer.transform.SetParent(transform);
			_renderer.transform.localPosition = Vector3.zero;
			_renderer.EnableHandles(false);
		}
	}
}