﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace VRModel {
	public class VertexRenderer : MonoBehaviour {
		[SerializeField] private Vertex vertex = new Vertex();
		[SerializeField] protected Vector3 lastPosition;
		[SerializeField] protected Vector3 lastNormal;

		protected Vector3 position { 
			get{
				if(transform.parent.GetComponent<MultiVertexRenderer>() != null)
					return transform.parent.localPosition;
				else return transform.localPosition;
			}
			set{
				if(transform.parent.GetComponent<MultiVertexRenderer>() != null)
					transform.parent.localPosition = value;
				else transform.localPosition = value;
			}
		}

		protected Vector3 forward {
			get {
				return transform.forward;
				if(transform.parent.GetComponent<MultiVertexRenderer>() != null)
					return transform.parent.worldToLocalMatrix.MultiplyVector(transform.forward);
				else 
					return transform.worldToLocalMatrix.MultiplyVector(transform.forward);
			}
			set {
				if(transform.parent.GetComponent<MultiVertexRenderer>() != null)
					transform.parent.localToWorldMatrix.MultiplyVector(value);
				else
					transform.localToWorldMatrix.MultiplyVector(value);
				transform.forward = value;
			}
		}
    
		private void Start(){
			lastPosition = vertex.position;
			lastNormal = vertex.normal;
			position = vertex.position;
			forward = vertex.normal;
		}

		private void Update(){
			if(lastPosition != position){
				vertex.position = position;
				vertex.OnMove();
				lastPosition = position;
			}
			if(lastNormal != forward){
				vertex.normal = forward;
				vertex.OnRotate();
				lastNormal = forward;
			}
		}

		public void SetVertex(Vertex _vertex) { 
			vertex = _vertex;
			vertex.renderer = this;
		}

		public void EnableHandles(bool enabled){
			GetComponent<MeshRenderer>().enabled = enabled;
			GetComponent<SphereCollider>().enabled = enabled;
			GetComponent<Grabbable>().enabled = enabled;
		}
	}
}