﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VRModel {
public class QuadMeshEditor : MeshEditor {
	[SerializeField] private List<Quad> l_quads = new List<Quad>();

	public Vector3[] vertices {
		get {
			List<Vertex> l_vertices = new List<Vertex>();
			foreach(Quad _quad in l_quads){
				foreach(Vertex _vertex in _quad.vertexInstances){
					if(!l_vertices.Contains(_vertex)) l_vertices.Add(_vertex);
				}
			}
			return Quad.GetVector3sFromVertexInstances(l_vertices.ToArray());
		}
	}

	public int[] triangles {
		get {
			List<int> result = new List<int>();
			foreach(Quad _quad in l_quads){
				result.AddRange(_quad.triangles);
			}
			return result.ToArray();
		}
	}

	public void AddQuad(Quad _quad, bool merge){
		l_quads.Add(_quad);
		foreach(Vertex _vertex in _quad.vertexInstances){
			if(!l_vertices.Contains(_vertex)){
				l_vertices.Add(_vertex);
			}
		}
	}

	//public void AddQuadMerged(Quad _quad) {
	//	l_quads.Add(_quad);
	//	foreach(Vertex _vertex in _quad.vertexInstances){
	//		foreach(Vertex _vertex2 in l_vertices){
	//			if(_vertex == _vertex2) break; // break if vertex is already in list
	//			if(_vertex.position == _vertex2.position){
	//				
	//			} else {
	//				l_vertices.Add(_vertex);
	//			}
	//		}
	//	}
	//}
	
	public override Mesh GenerateCube(float width, float height, float depth){
		Mesh cube = new Mesh();
		AddVertex(new Vertex(width/2, 	height/2, 	depth/2));
		AddVertex(new Vertex(width/2, 	height/2, 	-depth/2));
		AddVertex(new Vertex(-width/2, height/2, 	-depth/2));
		AddVertex(new Vertex(-width/2, height/2, 	depth/2));
		AddVertex(new Vertex(width/2, 	-height/2, 	depth/2));
		AddVertex(new Vertex(width/2, 	-height/2, 	-depth/2));
		AddVertex(new Vertex(-width/2, -height/2, 	depth/2));
		AddVertex(new Vertex(-width/2, -height/2, 	-depth/2));

		l_quads.Add(new Quad(l_vertices[0], l_vertices[1], l_vertices[2], l_vertices[3]));

		l_quads.Add(new Quad(l_vertices[5], l_vertices[4], l_vertices[1], l_vertices[0]));
		l_quads.Add(new Quad(l_vertices[6], l_vertices[5], l_vertices[0], l_vertices[3]));
		l_quads.Add(new Quad(l_vertices[7], l_vertices[6], l_vertices[3], l_vertices[2]));
		l_quads.Add(new Quad(l_vertices[4], l_vertices[7], l_vertices[2], l_vertices[1]));
		
		l_quads.Add(new Quad(l_vertices[4], l_vertices[5], l_vertices[6], l_vertices[7]));

		cube.vertices = vertices;
		cube.triangles = triangles;
		cube.RecalculateNormals();
		return cube;
	}
}
}