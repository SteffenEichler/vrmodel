﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(FixedJoint))]
public class Grab : MonoBehaviour {
	public SteamVR_Controller.Device Controller;
	[SerializeField] private SphereCollider grabCollider;
	[SerializeField] private Grabbable currentGrabbed;

	private void Start(){
		SteamVR_TrackedObject trackedDevice = GetComponent<SteamVR_TrackedObject>();
		Debug.Log(trackedDevice.index);
		Controller = SteamVR_Controller.Input((int)trackedDevice.index);
		CreateComponents();
	}

	private void Update(){
		if(Controller.GetHairTriggerDown()){
			foreach(Collider _col in Physics.OverlapSphere(grabCollider.transform.position, grabCollider.radius)){
				Grabbable grabbable = _col.GetComponent<Grabbable>();
				if(grabbable != null){
					if(grabbable.OnGrab(this)){
						currentGrabbed = grabbable;
					}
				}
			}
		}
		if(Controller.GetHairTriggerUp()){
			if(currentGrabbed != null){
				currentGrabbed.OnRelease(this); 
				currentGrabbed = null;
			}
		}
	}

	private float GetTriggerAxis(){
		return Controller.GetAxis(EVRButtonId.k_EButton_SteamVR_Trigger).x;
	}

	private void CreateComponents(){
		// Rigidbody
		
		Rigidbody rb = GetComponent<Rigidbody>() == null ? gameObject.AddComponent<Rigidbody>() : GetComponent<Rigidbody>();
		rb.isKinematic = true;

		// Collider
		GameObject newObj = new GameObject();
		newObj.name = "GrabCollider";
		grabCollider =  GetComponent<SphereCollider>() == null ? newObj.AddComponent<SphereCollider>() : newObj.GetComponent<SphereCollider>();
		grabCollider.radius = 0.1f;
		grabCollider.isTrigger = true;
		newObj.transform.SetParent(transform);
	}
}
