﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Grabbable : MonoBehaviour {

	private new Rigidbody rigidbody;
	[ReadOnly] public Grab currentGrabber;


	public bool isKinematic = false;



	private void Awake(){
		rigidbody = GetComponent<Rigidbody>();
	}
	public bool OnGrab(Grab grab){
		// fail if it is already grabbed;
		if(currentGrabber != null) return false;
		currentGrabber = grab;

		FixedJoint FJ = gameObject.AddComponent<FixedJoint>();
		FJ.connectedBody = grab.GetComponent<Rigidbody>();

		rigidbody.isKinematic = false;
		return true;
	}

	public void OnRelease(Grab grab){
		currentGrabber = null;
		rigidbody.isKinematic = isKinematic;
		rigidbody.velocity = grab.Controller.velocity;
		rigidbody.angularVelocity = grab.Controller.angularVelocity;

		DestroyImmediate(GetComponent<FixedJoint>());
	}

	void Update()
	{
	}
}
